# IdFM APIPortal PHP Library

A library to use the APIPortal of Île-de-France Mobilités (organization of public transportation in Paris area).

This API is in development.

_Note: This library is not official, and it is not provided by Île-de-France Mobilités staff._

## Supported API

- UnitaryRealTime:
  - StopMonitoring
- JourneyPlanner:
  - Places (partial - only q parameter and only stop_area returned)
  - StopPoints (with or without url, no query parameters implemented)
- Disruptions
  - LineReports
- Elevators
  - Elevator status

## Install

```
composer install jonathanmm/idfm-apiportal
```
