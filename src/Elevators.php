<?php

namespace IdFMAPIPortal;

use IdFMAPIPortal\Model\ElevatorsStatus;

class Elevators extends CsvEndpoint
{
    const baseUrl = 'https://data.iledefrance-mobilites.fr';
    const beginPath = '/api/explore/v2.1/catalog/datasets/';

    public function Status(): array
    {
        $url = self::baseUrl . self::beginPath . '/etat-des-ascenseurs/exports/csv';
        $result = $this->generateRequest($url);
        return (new ElevatorsStatus($result))->getStatus();
    }
}
