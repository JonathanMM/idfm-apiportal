<?php

namespace IdFMAPIPortal;

use IdFMAPIPortal\Model\LineReportsResponse;
use IdFMAPIPortal\Parameter\LineReportsParameter;

class Disruptions extends APIEndpoint
{
    const baseUrl = 'https://prim.iledefrance-mobilites.fr';
    const beginPath = '/marketplace/v2/navitia/line_reports';

    public function LineReports(string $uri, LineReportsParameter | null $parameters = null): LineReportsResponse
    {
        if (empty($uri)) {
            throw new \InvalidArgumentException("The uri can't be empty");
        }

        $url = self::baseUrl . self::beginPath . '/' . urlencode($uri) . '/line_reports';
        if (!is_null($parameters)) {
            $urlParameters = $parameters->getParameters();
            $url .= '?' . join("&", $urlParameters);
        }
        $result = $this->generateRequest($url);
        return new LineReportsResponse($result);
    }
}
