<?php

namespace IdFMAPIPortal;

use \IdFMAPIPortal\Credentials\APICredentials;

abstract class APIEndpoint
{
    private string $_token;

    /**
     * Constructor
     *
     * @param APICredentials $clientCredentials
     */
    public function __construct(APICredentials $clientCredentials)
    {
        if (is_null($clientCredentials)) {
            throw new \InvalidArgumentException("The client credentials can't be null");
        }
        $this->_token = $clientCredentials->token;
    }

    /**
     * Get access token
     *
     * @return String
     */
    public function getToken(): string
    {
        return $this->_token;
    }

    protected function generateRequest(string $url): object
    {
        $contexte = stream_context_create(array(
            'http' => array(
                'method' => 'GET',
                'header' => 'apikey: ' . $this->_token . "\r\n" .
                    'Accept-Encoding: gzip' . "\r\n" .
                    'Accept: application/json;',
            ),
        ));
        $data = gzdecode(file_get_contents($url, false, $contexte));
        return json_decode($data);
    }
}
