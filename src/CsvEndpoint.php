<?php

namespace IdFMAPIPortal;

use IdFMAPIPortal\Credentials\DataCredentials;

abstract class CsvEndpoint
{
    private string $_token;

    /**
     * Constructor
     *
     * @param APICredentials $clientCredentials
     */
    public function __construct(DataCredentials $clientCredentials)
    {
        if (is_null($clientCredentials)) {
            throw new \InvalidArgumentException("The client credentials can't be null");
        }
        $this->_token = $clientCredentials->token;
    }

    /**
     * Get access token
     *
     * @return String
     */
    public function getToken(): string
    {
        return $this->_token;
    }

    protected function generateRequest(string $url): array
    {
        $contexte = stream_context_create(array(
            'http' => array(
                'method' => 'GET',
                'header' => 'Authorization: apikey ' . $this->_token . "\r\n",
            ),
        ));
        $data = file_get_contents($url, false, $contexte);
        return explode("\n", $data);
    }
}
