<?php

namespace IdFMAPIPortal;

use \IdFMAPIPortal\Model\StopMonitoringResponse;

class UnitaryRealTime extends APIEndpoint
{
    const baseUrl = 'https://prim.iledefrance-mobilites.fr';
    const beginPath = '/marketplace';

    public function StopMonitoring(string $MonitoringRef): StopMonitoringResponse
    {
        if (empty($MonitoringRef)) {
            throw new \InvalidArgumentException("The monitoring ref can't be empty");
        }

        $url = self::baseUrl . self::beginPath . '/stop-monitoring?MonitoringRef=' . urlencode($MonitoringRef);
        $result = $this->generateRequest($url);
        return new StopMonitoringResponse($result);
    }
}
