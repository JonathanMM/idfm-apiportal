<?php

namespace IdFMAPIPortal\Parameter;

class Pagination
{
    public int | null $start_page;
    public int | null $count;

    public function getParameters(): array
    {
        $parameters = [];
        if (!is_null($this->start_page)) $parameters[] = 'start_page=' . $this->start_page;
        if (!is_null($this->count)) $parameters[] = 'count=' . $this->count;
        return $parameters;
    }
}
