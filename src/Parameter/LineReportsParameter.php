<?php

namespace IdFMAPIPortal\Parameter;

class LineReportsParameter extends Pagination
{
    public int | null $depth = null;
    public array | null $forbidden_uris;
    public bool | null $disable_geojson;
    public \DateTime | null $since;
    public \DateTime | null $until;
    public array | null $tags;
    public string | null $language;

    public function getParameters(): array
    {
        $parameters = parent::getParameters();
        if (!is_null($this->depth)) $parameters[] = 'depth=' . $this->depth;
        if (!is_null($this->forbidden_uris)) {
            foreach ($this->forbidden_uris as $forbiddenUri) {
                $parameters[] = 'forbidden_uris[]=' . urlencode($forbiddenUri);
            }
        }
        if (!is_null($this->disable_geojson)) $parameters[] = 'disable_geojson=' . $this->disable_geojson;
        if (!is_null($this->since)) $parameters[] = 'since=' . $this->since->format("c");
        if (!is_null($this->until)) $parameters[] = 'until=' . $this->until->format("c");
        if (!is_null($this->tags)) {
            foreach ($this->tags as $tag) {
                $parameters[] = 'tags[]=' . urlencode($tag);
            }
        }
        if (!is_null($this->language)) $parameters[] = 'language=' . $this->language;
        return $parameters;
    }
}
