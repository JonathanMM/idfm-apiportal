<?php

namespace IdFMAPIPortal;

use \IdFMAPIPortal\Model\PlacesResponse;
use \IdFMAPIPortal\Model\StopPointsResponse;

class JourneyPlanner extends APIEndpoint
{
    const baseUrl = 'https://prim.iledefrance-mobilites.fr';
    const beginPath = '/marketplace/navitia';
    const coveragePath = '/coverage/fr-idf';

    public function Places(string $q): PlacesResponse
    {
        if (empty($q)) {
            throw new \InvalidArgumentException("The query termes (q) can't be empty");
        }

        $url = self::baseUrl . self::beginPath . self::coveragePath . '/places?q=' . urlencode($q);
        $result = $this->generateRequest($url);
        return new PlacesResponse($result);
    }

    public function StopPoints(string $url = null): StopPointsResponse
    {
        $urlApi = self::baseUrl . self::beginPath . self::coveragePath . (is_null($url) ? '' : '/' . $url) . '/stop_points';
        $result = $this->generateRequest($urlApi);
        return new StopPointsResponse($result);
    }
}
