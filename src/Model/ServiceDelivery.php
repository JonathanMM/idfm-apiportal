<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class ServiceDelivery
{
    private $_ResponseTimestamp;
    private $_ProducerRef;
    private $_ResponseMessageIdentifier;
    private $_StopMonitoringDelivery;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_ResponseTimestamp = new \DateTime($jsonObject->ResponseTimestamp);
        $this->_ProducerRef = $jsonObject->ProducerRef;
        $this->_ResponseMessageIdentifier = $jsonObject->ResponseMessageIdentifier;
        $this->_StopMonitoringDelivery = array_map(function ($item) {
            return new StopMonitoringDelivery($item);
        }, $jsonObject->StopMonitoringDelivery);
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'ResponseTimestamp':
                return $this->_ResponseTimestamp;
            case 'ProducerRef':
                return $this->_ProducerRef;
            case 'ResponseMessageIdentifier':
                return $this->_ResponseMessageIdentifier;
            case 'StopMonitoringDelivery':
                return $this->_StopMonitoringDelivery;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'ResponseTimestamp':
                return !is_null($this->_ResponseTimestamp);
            case 'ProducerRef':
                return !is_null($this->_ProducerRef);
            case 'ResponseMessageIdentifier':
                return !is_null($this->_ResponseMessageIdentifier);
            case 'StopMonitoringDelivery':
                return !is_null($this->_StopMonitoringDelivery);
            default:
                return false;
        }
    }
}
