<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class StopPoint
{
    private $_codes;
    private $_name;
    private $_links;
    private $_coord;
    private $_label;
    private $_administrative_regions;
    private $_fare_zone;
    private $_id;
    private $_stop_area;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_codes = array_map(function ($item) {
            return new Code($item);
        }, $jsonObject->codes);
        $this->_name = $jsonObject->name;
        $this->_links = array_map(function ($item) {
            return new Link($item);
        }, $jsonObject->links);
        $this->_coord = new Coord($jsonObject->coord);
        $this->_label = $jsonObject->label;
        $this->_administrative_regions = array_map(function ($item) {
            return new AdministrativeRegion($item);
        }, $jsonObject->administrative_regions);
        $this->_fare_zone = new FareZone($jsonObject->fare_zone);
        $this->_id = $jsonObject->id;
        $this->_stop_area = new StopArea($jsonObject->stop_area);
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'codes':
                return $this->_codes;
            case 'name':
                return $this->_name;
            case 'links':
                return $this->_links;
            case 'coord':
                return $this->_coord;
            case 'label':
                return $this->_label;
            case 'administrative_regions':
                return $this->_administrative_regions;
            case 'fare_zone':
                return $this->_fare_zone;
            case 'id':
                return $this->_id;
            case 'stop_area':
                return $this->_stop_area;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'codes':
                return !is_null($this->_codes);
            case 'name':
                return !is_null($this->_name);
            case 'links':
                return !is_null($this->_links);
            case 'coord':
                return !is_null($this->_coord);
            case 'label':
                return !is_null($this->_label);
            case 'administrative_regions':
                return !is_null($this->_administrative_regions);
            case 'fare_zone':
                return !is_null($this->_fare_zone);
            case 'id':
                return !is_null($this->_id);
            case 'stop_area':
                return !is_null($this->_stop_area);
            default:
                return false;
        }
    }
}
