<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class TrainNumbers
{
    private $_TrainNumberRef;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_TrainNumberRef = array_map(function ($item) {
            return new TrainNumberRef($item);
        }, $jsonObject->TrainNumberRef);
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'TrainNumberRef':
                return $this->_TrainNumberRef;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'TrainNumberRef':
                return !is_null($this->_TrainNumberRef);
            default:
                return false;
        }
    }
}
