<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class StopMonitoringResponse
{
    private $_Siri;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_Siri = new Siri($jsonObject->Siri);
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'Siri':
                return $this->_Siri;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'Siri':
                return !is_null($this->_Siri);
            default:
                return false;
        }
    }
}
