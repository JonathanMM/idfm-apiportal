<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class Disruption
{
    private $_id;
    private $_disruption_id;
    private $_impact_id;
    private $_application_periods;
    private $_status;
    private $_updated_at;
    private $_tags;
    private $_cause;
    private $_category;
    private $_severity;
    private $_messages;
    private $_impacted_objects;
    private $_uri;
    private $_disruption_uri;
    private $_contributor;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_id = $jsonObject->id;
        $this->_disruption_id = $jsonObject->disruption_id;
        $this->_impact_id = $jsonObject->impact_id;

        $this->_application_periods = array_map(function ($item) {
            return new ApplicationPeriod($item);
        }, $jsonObject->application_periods);

        $this->_status = $jsonObject->status;
        $this->_updated_at = new \DateTime($jsonObject->updated_at);

        $this->_tags = array_map(function ($item) {
            return (string) $item;
        }, $jsonObject->tags);

        $this->_cause = $jsonObject->cause;
        $this->_category = $jsonObject->category;
        $this->_severity = new Severity($jsonObject->severity);

        $this->_messages = array_map(function ($item) {
            return new Message($item);
        }, $jsonObject->messages);

        $this->_impacted_objects = array_map(function ($item) {
            return new ImpactedObject($item);
        }, $jsonObject->impacted_objects);

        $this->_uri = $jsonObject->uri;
        $this->_disruption_uri = $jsonObject->disruption_uri;
        $this->_contributor = $jsonObject->contributor;
    }
    public function __get(string $property)
    {
        switch ($property) {
            case 'id':
                return $this->_id;
            case 'disruption_id':
                return $this->_disruption_id;
            case 'impact_id':
                return $this->_impact_id;
            case 'application_periods':
                return $this->_application_periods;
            case 'status':
                return $this->_status;
            case 'updated_at':
                return $this->_updated_at;
            case 'tags':
                return $this->_tags;
            case 'cause':
                return $this->_cause;
            case 'category':
                return $this->_category;
            case 'severity':
                return $this->_severity;
            case 'messages':
                return $this->_messages;
            case 'impacted_objects':
                return $this->_impacted_objects;
            case 'uri':
                return $this->_uri;
            case 'disruption_uri':
                return $this->_disruption_uri;
            case 'contributor':
                return $this->_contributor;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'id':
                return !is_null($this->_id);
            case 'disruption_id':
                return !is_null($this->_disruption_id);
            case 'impact_id':
                return !is_null($this->_impact_id);
            case 'application_periods':
                return !is_null($this->_application_periods);
            case 'status':
                return !is_null($this->_status);
            case 'updated_at':
                return !is_null($this->_updated_at);
            case 'tags':
                return !is_null($this->_tags);
            case 'cause':
                return !is_null($this->_cause);
            case 'category':
                return !is_null($this->_category);
            case 'severity':
                return !is_null($this->_severity);
            case 'messages':
                return !is_null($this->_messages);
            case 'impacted_objects':
                return !is_null($this->_impacted_objects);
            case 'uri':
                return !is_null($this->_uri);
            case 'disruption_uri':
                return !is_null($this->_disruption_uri);
            case 'contributor':
                return !is_null($this->_contributor);
            default:
                return false;
        }
    }
}
