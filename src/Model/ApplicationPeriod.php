<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class ApplicationPeriod
{
    private $_begin;
    private $_end;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_begin = new \DateTime($jsonObject->begin);
        $this->_end = new \DateTime($jsonObject->end);
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'begin':
                return $this->_begin;
            case 'end':
                return $this->_end;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'begin':
                return !is_null($this->_begin);
            case 'end':
                return !is_null($this->_end);
            default:
                return false;
        }
    }
}
