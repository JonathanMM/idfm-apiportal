<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class FareZone
{
    private $_name;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_name = $jsonObject->name;
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'name':
                return $this->_name;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'name':
                return !is_null($this->_name);
            default:
                return false;
        }
    }
}
