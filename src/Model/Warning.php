<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class Warning
{
    private $_message;
    private $_id;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_message = $jsonObject->message;
        $this->_id = $jsonObject->id;
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'message':
                return $this->_message;
            case 'id':
                return $this->_id;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'message':
                return !is_null($this->_message);
            case 'id':
                return !is_null($this->_id);
            default:
                return false;
        }
    }
}
