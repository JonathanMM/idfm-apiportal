<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class StopMonitoringDelivery
{
    private $_ResponseTimestamp;
    private $_Version;
    private $_Status;
    private $_MonitoredStopVisit;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_ResponseTimestamp = new \DateTime($jsonObject->ResponseTimestamp);
        $this->_Version = $jsonObject->Version;
        $this->_Status = strtolower($jsonObject->Status) == "true";
        $this->_MonitoredStopVisit = array_map(function ($item) {
            return new MonitoredStopVisit($item);
        }, $jsonObject->MonitoredStopVisit);
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'ResponseTimestamp':
                return $this->_ResponseTimestamp;
            case 'Version':
                return $this->_Version;
            case 'Status':
                return $this->_Status;
            case 'MonitoredStopVisit':
                return $this->_MonitoredStopVisit;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'ResponseTimestamp':
                return !is_null($this->_ResponseTimestamp);
            case 'Version':
                return !is_null($this->_Version);
            case 'Status':
                return !is_null($this->_Status);
            case 'MonitoredStopVisit':
                return !is_null($this->_MonitoredStopVisit);
            default:
                return false;
        }
    }
}
