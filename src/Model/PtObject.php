<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class PtObject
{
    private $_id;
    private $_name;
    private $_quality;
    private $_stop_area;
    private $_line;
    private $_embedded_type;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_id = $jsonObject->id;
        $this->_name = $jsonObject->name;
        $this->_quality = $jsonObject->quality;
        if (isset($jsonObject->stop_area)) $this->_stop_area = new StopArea($jsonObject->stop_area);
        if (isset($jsonObject->line)) $this->_line = new Line($jsonObject->line);
        $this->_embedded_type = $jsonObject->embedded_type;
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'id':
                return $this->_id;
            case 'name':
                return $this->_name;
            case 'quality':
                return $this->_quality;
            case 'stop_area':
                return $this->_stop_area;
            case 'line':
                return $this->_line;
            case 'embedded_type':
                return $this->_embedded_type;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'id':
                return !is_null($this->_id);
            case 'name':
                return !is_null($this->_name);
            case 'quality':
                return !is_null($this->_quality);
            case 'stop_area':
                return !is_null($this->_stop_area);
            case 'line':
                return !is_null($this->_line);
            case 'embedded_type':
                return !is_null($this->_embedded_type);
            default:
                return false;
        }
    }
}
