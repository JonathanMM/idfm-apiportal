<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class Pagination
{
    private $_start_page;
    private $_items_on_page;
    private $_items_per_page;
    private $_total_result;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_start_page = $jsonObject->start_page;
        $this->_items_on_page = $jsonObject->items_on_page;
        $this->_items_per_page = $jsonObject->items_per_page;
        $this->_total_result = $jsonObject->total_result;
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'start_page':
                return $this->_start_page;
            case 'items_on_page':
                return $this->_items_on_page;
            case 'items_per_page':
                return $this->_items_per_page;
            case 'total_result':
                return $this->_total_result;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'start_page':
                return !is_null($this->_start_page);
            case 'items_on_page':
                return !is_null($this->_items_on_page);
            case 'items_per_page':
                return !is_null($this->_items_per_page);
            case 'total_result':
                return !is_null($this->_total_result);
            default:
                return false;
        }
    }
}
