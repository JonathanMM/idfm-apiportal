<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class Channel
{
    private $_content_type;
    private $_id;
    private $_name;
    private $_types;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_content_type = $jsonObject->content_type;
        $this->_id = $jsonObject->id;
        $this->_name = $jsonObject->name;
        $this->_types = array_map(function ($item) {
            return (string) $item;
        }, $jsonObject->types);
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'content_type':
                return $this->_content_type;
            case 'id':
                return $this->_id;
            case 'name':
                return $this->_name;
            case 'types':
                return $this->_types;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'content_type':
                return !is_null($this->_content_type);
            case 'id':
                return !is_null($this->_id);
            case 'name':
                return !is_null($this->_name);
            case 'types':
                return !is_null($this->_types);
            default:
                return false;
        }
    }
}
