<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class Severity
{
    private $_name;
    private $_effect;
    private $_color;
    private $_priority;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_name = $jsonObject->name;
        $this->_effect = $jsonObject->effect;
        $this->_color = $jsonObject->color;
        $this->_priority = $jsonObject->priority;
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'name':
                return $this->_name;
            case 'effect':
                return $this->_effect;
            case 'color':
                return $this->_color;
            case 'priority':
                return $this->_priority;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'name':
                return !is_null($this->_name);
            case 'effect':
                return !is_null($this->_effect);
            case 'color':
                return !is_null($this->_color);
            case 'priority':
                return !is_null($this->_priority);
            default:
                return false;
        }
    }
}
