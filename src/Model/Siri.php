<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class Siri
{
    private $_ServiceDelivery;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_ServiceDelivery = new ServiceDelivery($jsonObject->ServiceDelivery);
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'ServiceDelivery':
                return $this->_ServiceDelivery;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'ServiceDelivery':
                return !is_null($this->_ServiceDelivery);
            default:
                return false;
        }
    }
}
