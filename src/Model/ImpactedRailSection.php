<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;
use IdFMAPIPortal\Model\PtObject;

class ImpactedRailSection
{
    private $_from;
    private $_to;
    private $_routes;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_from = new PtObject($jsonObject->from);
        $this->_to = new PtObject($jsonObject->to);
        $this->_routes = array_map(function ($item) {
            return new Route($item);
        }, $jsonObject->routes);
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'from':
                return $this->_from;
            case 'to':
                return $this->_to;
            case 'routes':
                return $this->_routes;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'from':
                return !is_null($this->_from);
            case 'to':
                return !is_null($this->_to);
            case 'routes':
                return !is_null($this->_routes);
            default:
                return false;
        }
    }
}
