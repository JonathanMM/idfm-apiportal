<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class LineReportsResponse
{
    private $_pagination;
    private $_links;
    private $_feed_publishers;
    private $_context;
    private $_disruptions;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_pagination = new Pagination($jsonObject->pagination);
        $this->_links = array_map(function ($item) {
            return new Link($item);
        }, $jsonObject->links);
        $this->_feed_publishers = array_map(function ($item) {
            return new FeedPublisher($item);
        }, $jsonObject->feed_publishers);
        $this->_context = new Context($jsonObject->context);
        $this->_disruptions = array_map(function ($item) {
            return new Disruption($item);
        }, $jsonObject->disruptions);
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'pagination':
                return $this->_pagination;
            case 'links':
                return $this->_links;
            case 'feed_publishers':
                return $this->_feed_publishers;
            case 'context':
                return $this->_context;
            case 'disruptions':
                return $this->_disruptions;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'pagination':
                return !is_null($this->_pagination);
            case 'links':
                return !is_null($this->_links);
            case 'feed_publishers':
                return !is_null($this->_feed_publishers);
            case 'context':
                return !is_null($this->_context);
            case 'disruptions':
                return !is_null($this->_disruptions);
            default:
                return false;
        }
    }
}
