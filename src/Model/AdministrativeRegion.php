<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class AdministrativeRegion
{
    private $_insee;
    private $_name;
    private $_level;
    private $_coord;
    private $_label;
    private $_id;
    private $_zip_code;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_insee = $jsonObject->insee;
        $this->_name = $jsonObject->name;
        $this->_level = $jsonObject->level;
        $this->_coord = new Coord($jsonObject->coord);
        $this->_label = $jsonObject->label;
        $this->_id = $jsonObject->id;
        if (isset($jsonObject->zip_code)) {
            $this->_zip_code = $jsonObject->zip_code;
        }
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'insee':
                return $this->_insee;
            case 'name':
                return $this->_name;
            case 'level':
                return $this->_level;
            case 'coord':
                return $this->_coord;
            case 'label':
                return $this->_label;
            case 'id':
                return $this->_id;
            case 'zip_code':
                return $this->_zip_code;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'insee':
                return !is_null($this->_insee);
            case 'name':
                return !is_null($this->_name);
            case 'level':
                return !is_null($this->_level);
            case 'coord':
                return !is_null($this->_coord);
            case 'label':
                return !is_null($this->_label);
            case 'id':
                return !is_null($this->_id);
            case 'zip_code':
                return !is_null($this->_zip_code);
            default:
                return false;
        }
    }
}
