<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class StopArea
{
    private $_comment;
    private $_commercial_modes;
    private $_name;
    private $_physical_modes;
    private $_lines;
    private $_coord;
    private $_label;
    private $_codes;
    private $_administrative_regions;
    private $_timezone;
    private $_id;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        if (isset($jsonObject->comment)) {
            $this->_comment = $jsonObject->comment;
        }

        if (isset($jsonObject->commercial_modes)) {
            $this->_commercial_modes = array_map(function ($item) {
                return new CommercialMode($item);
            }, $jsonObject->commercial_modes);
        }

        $this->_name = $jsonObject->name;

        if (isset($jsonObject->physical_modes)) {
            $this->_physical_modes = array_map(function ($item) {
                return new PhysicalMode($item);
            }, $jsonObject->physical_modes);
        }

        if (isset($jsonObject->lines)) {
            $this->_lines = array_map(function ($item) {
                return new Line($item);
            }, $jsonObject->lines);
        }

        $this->_coord = new Coord($jsonObject->coord);
        $this->_label = $jsonObject->label;
        $this->_codes = array_map(function ($item) {
            return new Code($item);
        }, $jsonObject->codes);

        if (isset($jsonObject->administrative_regions)) {
            $this->_administrative_regions = array_map(function ($item) {
                return new AdministrativeRegion($item);
            }, $jsonObject->administrative_regions);
        }

        $this->_timezone = $jsonObject->timezone;
        $this->_id = $jsonObject->id;
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'comment':
                return $this->_comment;
            case 'commercial_modes':
                return $this->_commercial_modes;
            case 'name':
                return $this->_name;
            case 'physical_modes':
                return $this->_physical_modes;
            case 'lines':
                return $this->_lines;
            case 'coord':
                return $this->_coord;
            case 'label':
                return $this->_label;
            case 'codes':
                return $this->_codes;
            case 'administrative_regions':
                return $this->_administrative_regions;
            case 'timezone':
                return $this->_timezone;
            case 'id':
                return $this->_id;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'comment':
                return !is_null($this->_comment);
            case 'commercial_modes':
                return !is_null($this->_commercial_modes);
            case 'name':
                return !is_null($this->_name);
            case 'physical_modes':
                return !is_null($this->_physical_modes);
            case 'lines':
                return !is_null($this->_lines);
            case 'coord':
                return !is_null($this->_coord);
            case 'label':
                return !is_null($this->_label);
            case 'codes':
                return !is_null($this->_codes);
            case 'administrative_regions':
                return !is_null($this->_administrative_regions);
            case 'timezone':
                return !is_null($this->_timezone);
            case 'id':
                return !is_null($this->_id);
            default:
                return false;
        }
    }
}
