<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class MonitoredStopVisit
{
    private $_RecordedAtTime;
    private $_ItemIdentifier;
    private $_MonitoringRef;
    private $_MonitoredVehicleJourney;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_RecordedAtTime = new \DateTime($jsonObject->RecordedAtTime);
        $this->_ItemIdentifier = $jsonObject->ItemIdentifier;
        $this->_MonitoringRef = new MonitoringRef($jsonObject->MonitoringRef);
        $this->_MonitoredVehicleJourney = new MonitoredVehicleJourney($jsonObject->MonitoredVehicleJourney);
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'RecordedAtTime':
                return $this->_RecordedAtTime;
            case 'ItemIdentifier':
                return $this->_ItemIdentifier;
            case 'MonitoringRef':
                return $this->_MonitoringRef;
            case 'MonitoredVehicleJourney':
                return $this->_MonitoredVehicleJourney;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'RecordedAtTime':
                return !is_null($this->_RecordedAtTime);
            case 'ItemIdentifier':
                return !is_null($this->_ItemIdentifier);
            case 'MonitoringRef':
                return !is_null($this->_MonitoringRef);
            case 'MonitoredVehicleJourney':
                return !is_null($this->_MonitoredVehicleJourney);
            default:
                return false;
        }
    }
}
