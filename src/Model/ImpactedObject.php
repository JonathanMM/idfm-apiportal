<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class ImpactedObject
{
    private $_pt_object;
    private $_impacted_rail_section;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_pt_object = new PtObject($jsonObject->pt_object);
        if (isset($jsonObject->impacted_rail_section))
            $this->_impacted_rail_section = new ImpactedRailSection($jsonObject->impacted_rail_section);
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'pt_object':
                return $this->_pt_object;
            case 'impacted_rail_section':
                return $this->_impacted_rail_section;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'pt_object':
                return !is_null($this->_pt_object);
            case 'impacted_rail_section':
                return !is_null($this->_impacted_rail_section);
            default:
                return false;
        }
    }
}
