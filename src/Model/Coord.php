<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class Coord
{
    private $_lat;
    private $_lon;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_lat = $jsonObject->lat;
        $this->_lon = $jsonObject->lon;
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'lat':
                return $this->_lat;
            case 'lon':
                return $this->_lon;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'lat':
                return !is_null($this->_lat);
            case 'lon':
                return !is_null($this->_lon);
            default:
                return false;
        }
    }
}
