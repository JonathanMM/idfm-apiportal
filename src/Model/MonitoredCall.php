<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class MonitoredCall
{
    private $_Order;
    private $_StopPointName;
    private $_VehicleAtStop;
    private $_DestinationDisplay;
    private $_AimedArrivalTime;
    private $_ExpectedArrivalTime;
    private $_ArrivalStatus;
    private $_ArrivalPlatformName;
    private $_AimedDepartureTime;
    private $_ExpectedDepartureTime;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_StopPointName = array_map(function ($item) {
            return new StopPointName($item);
        }, $jsonObject->StopPointName);
        $this->_VehicleAtStop = isset($jsonObject->VehicleAtStop) ? $jsonObject->VehicleAtStop : false;
        $this->_ExpectedDepartureTime = isset($jsonObject->ExpectedDepartureTime) ? new \DateTime($jsonObject->ExpectedDepartureTime) : null;
        $this->_Order = isset($jsonObject->Order) ? $jsonObject->Order : null;
        $this->_DestinationDisplay = isset($jsonObject->DestinationDisplay) ? array_map(function ($item) {
            return new DestinationDisplay($item);
        }, $jsonObject->DestinationDisplay) : null;
        $this->_AimedArrivalTime = isset($jsonObject->AimedArrivalTime) ? new \DateTime($jsonObject->AimedArrivalTime) : null;
        $this->_ExpectedArrivalTime = isset($jsonObject->ExpectedArrivalTime) ? new \DateTime($jsonObject->ExpectedArrivalTime) : null;
        $this->_ArrivalStatus = isset($jsonObject->ArrivalStatus) ? $jsonObject->ArrivalStatus : null;
        $this->_ArrivalPlatformName = isset($jsonObject->ArrivalPlatformName) ? new ArrivalPlatformName($jsonObject->ArrivalPlatformName) : null;
        $this->_AimedDepartureTime = isset($jsonObject->AimedDepartureTime) ? new \DateTime($jsonObject->AimedDepartureTime) : null;
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'StopPointName':
                return $this->_StopPointName;
            case 'VehicleAtStop':
                return $this->_VehicleAtStop;
            case 'ExpectedDepartureTime':
                return $this->_ExpectedDepartureTime;
            case 'Order':
                return $this->_Order;
            case 'DestinationDisplay':
                return $this->_DestinationDisplay;
            case 'AimedArrivalTime':
                return $this->_AimedArrivalTime;
            case 'ExpectedArrivalTime':
                return $this->_ExpectedArrivalTime;
            case 'AimedDepartureTime':
                return $this->_AimedDepartureTime;
            case 'ArrivalStatus':
                return $this->_ArrivalStatus;
            case 'ArrivalPlatformName':
                return $this->_ArrivalPlatformName;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'StopPointName':
                return !is_null($this->_StopPointName);
            case 'VehicleAtStop':
                return !is_null($this->_VehicleAtStop);
            case 'ExpectedDepartureTime':
                return !is_null($this->_ExpectedDepartureTime);
            case 'Order':
                return !is_null($this->_Order);
            case 'DestinationDisplay':
                return !is_null($this->_DestinationDisplay);
            case 'AimedArrivalTime':
                return !is_null($this->_AimedArrivalTime);
            case 'ExpectedArrivalTime':
                return !is_null($this->_ExpectedArrivalTime);
            case 'AimedDepartureTime':
                return !is_null($this->_AimedDepartureTime);
            case 'ArrivalStatus':
                return !is_null($this->_ArrivalStatus);
            case 'ArrivalPlatformName':
                return !is_null($this->_ArrivalPlatformName);
            default:
                return false;
        }
    }
}
