<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class Code
{
    private $_type;
    private $_value;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_type = $jsonObject->type;
        $this->_value = $jsonObject->value;
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'type':
                return $this->_type;
            case 'value':
                return $this->_value;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'type':
                return !is_null($this->_type);
            case 'value':
                return !is_null($this->_value);
            default:
                return false;
        }
    }
}
