<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class FeedPublisher
{
    private $_url;
    private $_id;
    private $_license;
    private $_name;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_url = $jsonObject->url;
        $this->_id = $jsonObject->id;
        $this->_license = $jsonObject->license;
        $this->_name = $jsonObject->name;
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'url':
                return $this->_url;
            case 'id':
                return $this->_id;
            case 'license':
                return $this->_license;
            case 'name':
                return $this->_name;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'url':
                return !is_null($this->_url);
            case 'id':
                return !is_null($this->_id);
            case 'license':
                return !is_null($this->_license);
            case 'name':
                return !is_null($this->_name);
            default:
                return false;
        }
    }
}
