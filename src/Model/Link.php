<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class Link
{
    private $_href;
    private $_type;
    private $_rel;
    private $_templated;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_href = $jsonObject->href;
        $this->_type = $jsonObject->type;
        if (isset($jsonObject->rel)) {
            $this->_rel = $jsonObject->rel;
        }

        $this->_templated = $jsonObject->templated;
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'href':
                return $this->_href;
            case 'type':
                return $this->_type;
            case 'rel':
                return $this->_rel;
            case 'templated':
                return $this->_templated;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'href':
                return !is_null($this->_href);
            case 'type':
                return !is_null($this->_type);
            case 'rel':
                return !is_null($this->_rel);
            case 'templated':
                return !is_null($this->_templated);
            default:
                return false;
        }
    }
}
