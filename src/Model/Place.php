<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class Place
{
    private $_embedded_type;
    private $_stop_area;
    private $_quality;
    private $_name;
    private $_id;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_embedded_type = $jsonObject->embedded_type;

        if (isset($jsonObject->stop_area)) {
            $this->_stop_area = new StopArea($jsonObject->stop_area);
        }

        $this->_quality = $jsonObject->quality;
        $this->_name = $jsonObject->name;
        $this->_id = $jsonObject->id;
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'embedded_type':
                return $this->_embedded_type;
            case 'stop_area':
                return $this->_stop_area;
            case 'quality':
                return $this->_quality;
            case 'name':
                return $this->_name;
            case 'id':
                return $this->_id;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'embedded_type':
                return !is_null($this->_embedded_type);
            case 'stop_area':
                return !is_null($this->_stop_area);
            case 'quality':
                return !is_null($this->_quality);
            case 'name':
                return !is_null($this->_name);
            case 'id':
                return !is_null($this->_id);
            default:
                return false;
        }
    }
}
