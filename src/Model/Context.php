<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class Context
{
    private $_timezone;
    private $_current_datetime;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_timezone = $jsonObject->timezone;
        $this->_current_datetime = $jsonObject->current_datetime;
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'timezone':
                return $this->_timezone;
            case 'current_datetime':
                return $this->_current_datetime;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'timezone':
                return !is_null($this->_timezone);
            case 'current_datetime':
                return !is_null($this->_current_datetime);
            default:
                return false;
        }
    }
}
