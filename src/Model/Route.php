<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class Route
{
    private $_id;
    private $_name;
    private $_is_frequence;
    private $_direction_type;
    private $_codes;
    private $_direction;
    private $_links;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_id = $jsonObject->id;
        $this->_name = $jsonObject->name;
        $this->_is_frequence = strtolower($jsonObject->is_frequence) == "true";
        $this->_direction_type = $jsonObject->direction_type;
        if (isset($jsonObject->codes)) {
            $this->_codes = array_map(function ($item) {
                return new Code($item);
            }, $jsonObject->codes);
        }
        $this->_direction = new PtObject($jsonObject->direction);
        $this->_links = [];
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'id':
                return $this->_id;
            case 'name':
                return $this->_name;
            case 'is_frequence':
                return $this->_is_frequence;
            case 'direction_type':
                return $this->_direction_type;
            case 'codes':
                return $this->_codes;
            case 'direction':
                return $this->_direction;
            case 'links_links':
                return $this->_links;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'id':
                return !is_null($this->_id);
            case 'name':
                return !is_null($this->_name);
            case 'is_frequence':
                return !is_null($this->_is_frequence);
            case 'direction_type':
                return !is_null($this->_direction_type);
            case 'codes':
                return !is_null($this->_codes);
            case 'direction':
                return !is_null($this->_direction);
            case 'links':
                return !is_null($this->_links);
            default:
                return false;
        }
    }
}
