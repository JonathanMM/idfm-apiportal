<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\MissingDataException;

class ElevatorsStatus
{
    private array $_status;

    public function __construct(array $rows)
    {
        $this->_status = $this->extractRows($rows);
    }

    public function getStatus(): array
    {
        return $this->_status;
    }

    private function extractRows(array $rows): array
    {
        if (count($rows) == 0) {
            throw new MissingDataException("Data missing");
        }

        $rowHeader = explode(";", $rows[0]);
        $headers = array();
        foreach ($rowHeader as $column => $header) {
            $headers[$column] = trim($header, " \n\r\t\v\x00\xEF\xBB\xBF");
        }

        $datas = array();
        foreach (array_slice($rows, 1) as $row) {
            if (strlen(trim($row)) <= 1) {
                continue;
            }

            $cells = explode(";", $row);
            $data = array();
            foreach ($cells as $i => $cell) {
                $data[$headers[$i]] = trim($cell, " \n\r\t\v\x00\xEF\xBB\xBF");
            }
            $datas[] = new ElevatorStatus($data);
        }
        return $datas;
    }
}
