<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class Line
{
    private $_code;
    private $_name;
    private $_physical_modes;
    private $_commercial_mode;
    private $_color;
    private $_text_color;
    private $_id;
    private $_network;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_code = $jsonObject->code;
        $this->_name = $jsonObject->name;
        $this->_physical_modes = array_map(function ($item) {
            return new PhysicalMode($item);
        }, $jsonObject->physical_modes);
        $this->_commercial_mode = new CommercialMode($jsonObject->commercial_mode);
        $this->_color = $jsonObject->color;
        $this->_text_color = $jsonObject->text_color;
        $this->_id = $jsonObject->id;
        $this->_network = new Network($jsonObject->network);
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'code':
                return $this->_code;
            case 'name':
                return $this->_name;
            case 'physical_modes':
                return $this->_physical_modes;
            case 'commercial_mode':
                return $this->_commercial_mode;
            case 'color':
                return $this->_color;
            case 'text_color':
                return $this->_text_color;
            case 'id':
                return $this->_id;
            case 'network':
                return $this->_network;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'code':
                return !is_null($this->_code);
            case 'name':
                return !is_null($this->_name);
            case 'physical_modes':
                return !is_null($this->_physical_modes);
            case 'commercial_mode':
                return !is_null($this->_commercial_mode);
            case 'color':
                return !is_null($this->_color);
            case 'text_color':
                return !is_null($this->_text_color);
            case 'id':
                return !is_null($this->_id);
            case 'network':
                return !is_null($this->_network);
            default:
                return false;
        }
    }
}
