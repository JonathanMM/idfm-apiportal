<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class ElevatorStatus
{
    private string $_liftId;
    private string $_liftStateUpdate;
    private string $_privateElevatorId;
    private string $_liftSituation;
    private string $_liftMode;
    private string $_liftDirection;
    private string $_liftStatus;
    private string $_liftState;
    private string $_liftReason;
    private string $_zdcId;
    private string $_zdcName;
    private int $_zdcXEpsg2154;
    private int $_zdcYEpsg2154;
    private string $_centroidZdc;

    public function __construct(array $row)
    {
        if (is_null($row)) {
            throw new \InvalidArgumentException("The row can't be null");
        }

        $this->_zdcId = $row["zdcid"];
        $this->_zdcXEpsg2154 = $row["zdcxepsg2154"];
        $this->_zdcYEpsg2154 = $row["zdcyepsg2154"];
        $this->_liftId = $row["liftid"];
        $this->_liftStateUpdate = $row["liftstateupdate"];
        $this->_liftSituation = $row["liftsituation"];
        $this->_liftState = $row["liftstate"];
        $this->_liftMode = $row["liftmode"];
        $this->_liftDirection = $row["liftdirection"];
        $this->_liftStatus = $row["liftstatus"];
        $this->_liftReason = $row["liftreason"];
        $this->_zdcName = $row["zdcname"];
        $this->_centroidZdc = $row["centroidzdc"];
        $this->_privateElevatorId = $row["privateelevatorid"];
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'liftId':
                return $this->_liftId;
            case 'liftStateUpdate':
                return $this->_liftStateUpdate;
            case 'privateElevatorId':
                return $this->_privateElevatorId;
            case 'liftSituation':
                return $this->_liftSituation;
            case 'liftMode':
                return $this->_liftMode;
            case 'liftDirection':
                return $this->_liftDirection;
            case 'liftStatus':
                return $this->_liftStatus;
            case 'liftState':
                return $this->_liftState;
            case 'liftReason':
                return $this->_liftReason;
            case 'zdcId':
                return $this->_zdcId;
            case 'zdcName':
                return $this->_zdcName;
            case 'zdcXEpsg2154':
                return $this->_zdcXEpsg2154;
            case 'zdcYEpsg2154':
                return $this->_zdcYEpsg2154;
            case 'centroidZdc':
                return $this->_centroidZdc;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'liftId':
                return !is_null($this->_liftId);
            case 'liftStateUpdate':
                return !is_null($this->_liftStateUpdate);
            case 'privateElevatorId':
                return !is_null($this->_privateElevatorId);
            case 'liftSituation':
                return !is_null($this->_liftSituation);
            case 'liftMode':
                return !is_null($this->_liftMode);
            case 'liftDirection':
                return !is_null($this->_liftDirection);
            case 'liftStatus':
                return !is_null($this->_liftStatus);
            case 'liftState':
                return !is_null($this->_liftState);
            case 'liftReason':
                return !is_null($this->_liftReason);
            case 'zdcId':
                return !is_null($this->_zdcId);
            case 'zdcName':
                return !is_null($this->_zdcName);
            case 'zdcXEpsg2154':
                return !is_null($this->_zdcXEpsg2154);
            case 'zdcYEpsg2154':
                return !is_null($this->_zdcYEpsg2154);
            case 'centroidZdc':
                return !is_null($this->_centroidZdc);
            default:
                return false;
        }
    }
}
