<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class PlacesResponse
{
    private $_feed_publishers;
    private $_links;
    private $_places;
    private $_warnings;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_feed_publishers = array_map(function ($item) {
            return new FeedPublisher($item);
        }, $jsonObject->feed_publishers);
        $this->_links = array_map(function ($item) {
            return new Link($item);
        }, $jsonObject->links);
        $this->_places = array_map(function ($item) {
            return new Place($item);
        }, $jsonObject->places);
        $this->_warnings = array_map(function ($item) {
            return new Warning($item);
        }, $jsonObject->warnings);
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'feed_publishers':
                return $this->_feed_publishers;
            case 'links':
                return $this->_links;
            case 'places':
                return $this->_places;
            case 'warnings':
                return $this->_warnings;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'feed_publishers':
                return !is_null($this->_feed_publishers);
            case 'links':
                return !is_null($this->_links);
            case 'places':
                return !is_null($this->_places);
            case 'warnings':
                return !is_null($this->_warnings);
            default:
                return false;
        }
    }
}
