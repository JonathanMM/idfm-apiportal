<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class MonitoredVehicleJourney
{
    private $_LineRef;
    private $_OperatorRef;
    private $_FramedVehicleJourneyRef;
    private $_DestinationRef;
    private $_DestinationName;
    private $_JourneyNote;
    private $_TrainNumbers;
    private $_MonitoredCall;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_LineRef = new LineRef($jsonObject->LineRef);
        $this->_FramedVehicleJourneyRef = new FramedVehicleJourneyRef($jsonObject->FramedVehicleJourneyRef);
        $this->_DestinationRef = new DestinationRef($jsonObject->DestinationRef);
        $this->_DestinationName = array_map(function ($item) {
            return new DestinationName($item);
        }, $jsonObject->DestinationName);
        $this->_MonitoredCall = new MonitoredCall($jsonObject->MonitoredCall);
        $this->_OperatorRef = isset($jsonObject->OperatorRef) ? new OperatorRef($jsonObject->OperatorRef) : null;
        $this->_JourneyNote = isset($jsonObject->JourneyNote) ? array_map(function ($item) {
            return new JourneyNote($item);
        }, $jsonObject->JourneyNote) : null;
        $this->_TrainNumbers = isset($jsonObject->TrainNumbers) ? new TrainNumbers($jsonObject->TrainNumbers) : null;
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'LineRef':
                return $this->_LineRef;
            case 'FramedVehicleJourneyRef':
                return $this->_FramedVehicleJourneyRef;
            case 'DestinationRef':
                return $this->_DestinationRef;
            case 'DestinationName':
                return $this->_DestinationName;
            case 'MonitoredCall':
                return $this->_MonitoredCall;
            case 'OperatorRef':
                return $this->_OperatorRef;
            case 'JourneyNote':
                return $this->_JourneyNote;
            case 'TrainNumbers':
                return $this->_TrainNumbers;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'LineRef':
                return !is_null($this->_LineRef);
            case 'FramedVehicleJourneyRef':
                return !is_null($this->_FramedVehicleJourneyRef);
            case 'DestinationRef':
                return !is_null($this->_DestinationRef);
            case 'DestinationName':
                return !is_null($this->_DestinationName);
            case 'MonitoredCall':
                return !is_null($this->_MonitoredCall);
            case 'OperatorRef':
                return !is_null($this->_OperatorRef);
            case 'JourneyNote':
                return !is_null($this->_JourneyNote);
            case 'TrainNumbers':
                return !is_null($this->_TrainNumbers);
            default:
                return false;
        }
    }
}
