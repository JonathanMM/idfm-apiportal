<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class FramedVehicleJourneyRef
{
    private $_DataFrameRef;
    private $_DatedVehicleJourneyRef;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_DataFrameRef = new DataFrameRef($jsonObject->DataFrameRef);
        $this->_DatedVehicleJourneyRef = $jsonObject->DatedVehicleJourneyRef;
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'DataFrameRef':
                return $this->_DataFrameRef;
            case 'DatedVehicleJourneyRef':
                return $this->_DatedVehicleJourneyRef;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'DataFrameRef':
                return !is_null($this->_DataFrameRef);
            case 'DatedVehicleJourneyRef':
                return !is_null($this->_DatedVehicleJourneyRef);
            default:
                return false;
        }
    }
}
