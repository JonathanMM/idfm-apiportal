<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class LineRef
{
    private $_value;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_value = $jsonObject->value;
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'value':
                return $this->_value;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'value':
                return !is_null($this->_value);
            default:
                return false;
        }
    }
}
