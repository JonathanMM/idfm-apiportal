<?php

namespace IdFMAPIPortal\Model;

use IdFMAPIPortal\Exception\ReadOnlyException;

class Message
{
    private $_text;
    private $_channel;

    public function __construct(object $jsonObject)
    {
        if (is_null($jsonObject)) {
            throw new \InvalidArgumentException("The json object can't be null");
        }

        $this->_text = $jsonObject->text;
        $this->_channel = new Channel($jsonObject->channel);
    }

    public function __get(string $property)
    {
        switch ($property) {
            case 'text':
                return $this->_text;
            case 'channel':
                return $this->_channel;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, object $value): void
    {
        throw new ReadOnlyException("The property is readonly");
    }

    public function __isset(string $property): bool
    {
        switch ($property) {
            case 'text':
                return !is_null($this->_text);
            case 'channel':
                return !is_null($this->_channel);
            default:
                return false;
        }
    }
}
