<?php

namespace IdFMAPIPortal\Credentials;

class DataCredentials
{
    private string $_token;

    public function __construct(string $token)
    {
        if (empty($token)) {
            throw new \InvalidArgumentException("The token can't be empty");
        }

        $this->_token = $token;
    }

    public function __get(string $property): string
    {
        switch ($property) {
            case 'token':
                return $this->_token;
            default:
                throw new \InvalidArgumentException();
        }
    }

    public function __set(string $property, string $value): void
    {
        throw new \BadMethodCallException("The property is readonly");
    }
}
