<?php

use PHPUnit\Framework\TestCase;

final class JourneyPlannerTest extends TestCase
{
    public function testCallJourneyPlannerReturnsAnObject(): void
    {
        $credentialsTamaJourneyPlanner = new \IdFMAPIPortal\Credentials\APICredentials('xxx');
        $api = new \IdFMAPIPortal\JourneyPlanner($credentialsTamaJourneyPlanner);
        $nomGare = "Goussainville";
        $donnees = $api->Places($nomGare);
        var_dump($donnees);
        $this->assertNotNull($donnees);
    }
}
