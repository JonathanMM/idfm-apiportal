<?php

use PHPUnit\Framework\TestCase;

final class ElevatorsTest extends TestCase
{
    public function testCanHaveDistruptions(): void
    {
        $credentials = new \IdFMAPIPortal\Credentials\DataCredentials('xxx');
        $api = new \IdFMAPIPortal\Elevators($credentials);
        $donnees = $api->Status();
        $this->assertNotNull($donnees);
        foreach ($donnees as $elevator) {
            echo $elevator->liftId . "\n";
            echo $elevator->zdcName . "\n";
            echo $elevator->liftDirection . "\n";
            echo $elevator->liftStatus . "\n";
            echo $elevator->liftState . "\n";
            echo $elevator->liftStateUpdate . "\n";
            echo "------------------" . "\n";
        }
    }
}
