<?php

use PHPUnit\Framework\TestCase;

final class DisruptionsTest extends TestCase
{
    public function testCanHaveDistruptions(): void
    {
        $credentialsTamaJourneyPlanner = new \IdFMAPIPortal\Credentials\APICredentials('xxx');
        $api = new \IdFMAPIPortal\Disruptions($credentialsTamaJourneyPlanner);
        $rerDUri = "lines/line:IDFM:C01728";
        $donnees = $api->LineReports($rerDUri);
        // var_dump($donnees);
        $this->assertNotNull($donnees);
        foreach ($donnees->disruptions as $disruption) {
            echo $disruption->id . "\n";
            foreach ($disruption->application_periods as $periode) {
                echo $periode->begin->format("c") . ' -> ' . $periode->end->format("c") . "\n";
            }
            echo "Messages" . "\n";
            foreach ($disruption->messages as $message) {
                echo "- " . $message->channel->name . ": \n";
                echo $message->text . "\n";
            }
            echo "------------------" . "\n";
        }
    }
}
